# -------------------------------------------------------------------------
# *
# * Makefile
# *
# * Copyright (C) 2015, Anthony Kleerekoper, University of Manchester
# * 
# * This file is part of PostQuePP.
# *
# * PostQuePP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * PostQuePP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with PostQuePP.  If not, see <http://www.gnu.org/licenses/>.
# *
# * This file was produced as part of the AXLE Project which has received funding from the European Union's
# * Seventh Framework Programme (FP7/2007-2013) under grant agreement n° 318633
# *
# * IDENTIFICATION
# *		contrib/postquepp/Makefile
# *
# * -------------------------------------------------------------------------
# */

# contrib/postquepp/Makefile

MODULE_big = postquepp
OBJS = postquepp.o $(WIN32RES)
PGFILEDESC = "postquepp - Predict the Execution Time of an SQL statement"
SHLIB_LINK += -lpython2.7

ifdef USE_PGXS
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS) 
else
subdir = contrib/postquepp
top_builddir = ../..
include $(top_builddir)/src/Makefile.global
include $(top_srcdir)/contrib/contrib-global.mk
endif
