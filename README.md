	PostQuePP

What is it?
-----------

PostQuePP (for PostgreSQL Query Performance Prediction) is an extension to PostgreSQL that can predict the execution time
of queries from the run-time of previously run queries. It does this using a simple nearest-neighbour regression method
based on the planner/optimizer cost of the query execution tree. It makes use of the planner hook and uses an external 
Python module to make the prediction.

PostQuePP was developed as part of the AXLE Project which has received funding from the European Union's Seventh Framework
Programme (FP7/2007-2013) under grant agreement n° 318633. 

Installation
------------

To install and use PostQuePP:
1. Copy the directory to your $PGHOME/contrib directory
2. Edit the absolute paths in postquepp.c and postquepp.py
3. Run make and make install inside the postquepp directory
4. Edit the postgresql.conf file to include postquepp as a preloaded library and restart PostgreSQL

After these steps, whenever you run a query it will be accompanied by a prediction. If the prediction is -1.0ms then there 
is not enough training data to make a prediction. The accuracy of the prediction depends on the number and type of queries
that have been run before.

Authors
------------

The code was written by Anthony Kleerekoper whilst at The University of Manchester. He can be contacted at 
anthonykleerekoper@gmail.com or a.kleerekoper@mmu.ac.uk

Copyright
-------

Copyright (c) 2015 Anthony Kleerekoper, University of Manchester

PostQuePP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PostQuePP is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PostQuePP.  If not, see <http://www.gnu.org/licenses/>.
