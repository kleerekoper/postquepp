/* -------------------------------------------------------------------------
 *
 * postquepp.c
 *
 * Copyright (C) 2015, Anthony Kleerekoper, University of Manchester
 * This file is part of PostQuePP.
 *
 * PostQuePP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PostQuePP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PostQuePP.  If not, see <http://www.gnu.org/licenses/>.
 * This file was produced as part of the AXLE Project which has received funding from the European Union's
 * Seventh Framework Programme (FP7/2007-2013) under grant agreement n° 318633
 *
 * IDENTIFICATION
 *contrib/postquepp/postquepp.c
 *
 * -------------------------------------------------------------------------
 */


#include "postgres.h"
#include "nodes/nodes.h"
#include "nodes/relation.h"
#include "utils/guc.h"
#include "optimizer/cost.h"
#include "optimizer/planner.h"
#include "parser/parsetree.h"
#include "utils/lsyscache.h"
#include <stdio.h>
#include "executor/instrument.h"
#include "commands/explain.h"

#include <python2.7/Python.h>


PG_MODULE_MAGIC;

//clock_t begin, end;
//double time_spent;

void _PG_init(void);
void _PG_fini(void);

/* Saved hook values in case of unload */
static planner_hook_type prev_planner = NULL;
static ExecutorEnd_hook_type prev_ExecutorEnd = NULL;

static PlannedStmt * postquepp_func(Query *parse, int cursorOptions, ParamListInfo boundParams);
static void feedback_func(QueryDesc *queryDesc);

void
_PG_init(void)
{
	/* Install hooks */
	prev_planner = planner_hook;
	planner_hook = postquepp_func;
	prev_ExecutorEnd = ExecutorEnd_hook;
	ExecutorEnd_hook = feedback_func;
}
void
_PG_fini(void)
{
	/* Uninstall hooks. */
	planner_hook = prev_planner;
	ExecutorEnd_hook = prev_ExecutorEnd;
}

static PlannedStmt * postquepp_func(Query *parse, int cursorOptions, ParamListInfo boundParams){
	PlannedStmt *result;
	Cost total_cost;
	double predictedExecTime;
	FILE*        exp_file;
	PyObject*    main_module, * global_dict, * expression, * pyresult;
	result = standard_planner(parse, cursorOptions, boundParams);

	total_cost = result->planTree->total_cost;
	//printf("Plan total cost = %0.1f\n",total_cost);

	Py_Initialize();
	exp_file = fopen("/home/anthony/Documents/postgresql-9.4.4/contrib/postquepp/postquepp.py", "r");
	PyRun_SimpleFile(exp_file, "");
	main_module = PyImport_AddModule("__main__");
	global_dict = PyModule_GetDict(main_module);
	expression = PyDict_GetItemString(global_dict, "predict");
	pyresult = PyObject_CallFunction(expression,"d",total_cost);
	predictedExecTime = PyFloat_AsDouble(pyresult);
//	Py_Finalize();

	printf("\n\n\tPredicted Execution Time:\t%0.1fms\n\n",predictedExecTime);

	//begin = clock();
	return result;
}

static void feedback_func(QueryDesc *queryDesc){
	FILE*        exp_file;
	PyObject*    main_module, * global_dict, * expression, * pyresult;
	//Cost total_cost;
	//double total_time;

	PlannedStmt	*result = queryDesc->plannedstmt;
    //printf("doing feedback\n");
	//const char *queryString = "";
	ExplainState es;
	ExplainInitState(&es);
	es.format = EXPLAIN_FORMAT_XML;
	es.pstmt = result;
	es.verbose = false;
	es.analyze = true;
	es.costs = true;
	es.buffers = false;
	es.timing = true;
	ExplainPrintPlan(&es, queryDesc);

/*
	end = clock();
	total_cost = queryDesc->plannedstmt->planTree->total_cost;
	if (queryDesc->totaltime == NULL){
		total_time = 1000.0*((double)(end - begin) / CLOCKS_PER_SEC);
	}else{
		total_time = queryDesc->totaltime->total;
	}
	printf("total Execution Time was %0.3fms\n",total_time);
*/

//	Py_Initialize();
	exp_file = fopen("/home/anthony/Documents/postgresql-9.4.4/contrib/postquepp/postquepp.py", "r");
	PyRun_SimpleFile(exp_file, "");
	main_module = PyImport_AddModule("__main__");
	global_dict = PyModule_GetDict(main_module);
	expression = PyDict_GetItemString(global_dict, "feedback");
	pyresult = PyObject_CallFunction(expression,"s",es.str->data);
//	Py_Finalize();


	if (prev_ExecutorEnd)
		prev_ExecutorEnd(queryDesc);
	else
		standard_ExecutorEnd(queryDesc);
}
