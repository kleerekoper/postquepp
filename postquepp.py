# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# *
# * postquepp.py
# *
# * Copyright (C) 2015, Anthony Kleerekoper, University of Manchester
# * This file is part of PostQuePP.
# *
# * PostQuePP is free software: you can redistribute it and/or modify
# * it under the terms of the GNU General Public License as published by
# * the Free Software Foundation, either version 3 of the License, or
# * (at your option) any later version.
# *
# * PostQuePP is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU General Public License for more details.
# *
# * You should have received a copy of the GNU General Public License
# * along with PostQuePP.  If not, see <http://www.gnu.org/licenses/>.
# *
# * This file was produced as part of the AXLE Project which has received funding from the European Union's
# * Seventh Framework Programme (FP7/2007-2013) under grant agreement n° 318633
# *
# * IDENTIFICATION
# *contrib/postquepp/postquepp.py
# *
# * -------------------------------------------------------------------------
# */

#! /usr/bin/python


import sys
import numpy as np
from sklearn import neighbors
import xml.etree.ElementTree as ET

def getCost(plan):
	totalCost = float(plan.find('Total-Cost').text)
	return totalCost

def getExecTime(plan):
	time = float(plan.find("Actual-Total-Time").text)
	#time = float(root.find("Total-Runtime").text)
	return time

def predict(cost):
    trainingFileName = "/home/anthony/Documents/postgresql-9.4.4/contrib/postquepp/trainingData.csv"
    data = np.loadtxt(open(trainingFileName,"rb"),delimiter=",")
    X = data[:,0]
    y = data[:,1]
    weights = 'distance'
    knn = neighbors.KNeighborsRegressor(5, weights=weights)
    model = knn.fit(X.reshape(X.shape[0],1), y.reshape(y.shape[0],1))
    testData = np.zeros(1)
    testData[0] = cost
    testData = testData.reshape(1,1)
    predictedTime = model.predict(testData)
    return predictedTime

def feedback(xmlString):
    root = ET.fromstring(xmlString)
    execTime = getExecTime(root)
    totalCost = getCost(root)
    #print "\tActual Execution Time:\t"+str(execTime)+"ms"
    trainingFileName = "/home/anthony/Documents/postgresql-9.4.4/contrib/postquepp/trainingData.csv"
    f = open(trainingFileName,"a")
    f.write(str(totalCost)+","+str(execTime)+"\n")
    #print "writing: "+str(totalCost)+","+str(execTime)
    f.flush()
    f.close()
